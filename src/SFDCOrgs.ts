import {
    AuthInfo,
    Config,
    Org,
    OrgConfigProperties
} from "@salesforce/core";

/**
 * Provides methods for accessing information about authorized Salesforce Orgs
 */
export class SFDCOrgs {

    // Cache the last username and Org that was accessed
    private static cachedUsername: string;
    private static cachedOrg: Org;

    /**
     * Retrieves a configuration value
     * @param global If true, retrieves from the global configuration. If false (default), rerieves from the local project's config
     * @param projectFolder The path to the folder for the SFDX project, if not retrieving a global value
     * @param configKey The configuration value to retrieve
     */
    static async getConfigValue(global: boolean, projectFolder: string, configKey: string) {
        let configValue = '';
        const configOptions = (global ? { isGlobal: true } : { isGlobal: false, rootFolder: projectFolder });
        const config = await Config.create(configOptions); // load the config located in the requested -- Config.create automatically looks for the ".sfdx/sfdx-config.json" path
        // const configExists = await config.exists();
        // if (configExists) {
            if (config.has(configKey)) {
                configValue = config.get(configKey)?.toString(); // get the Default Username config value
            }
        // }
        return Promise.resolve(configValue);
    }

    /**
     * Retrieves the default Dev Hub username
     * @param global If true, retrieves from the global configuration. If false (default), rerieves from the local project's config
     * @param projectFolder The path to the folder for the SFDX project, if not retrieving a global value
     */
    static async getDefaultDevHubUsername(global: boolean = false, projectFolder: string) {
        return SFDCOrgs.getConfigValue(global, projectFolder, OrgConfigProperties.TARGET_DEV_HUB);
    }

    /**
     * Retrieves the default username
     * @param global If true, retrieves from the global configuration. If false (default), rerieves from the local project's config
     * @param projectFolder The path to the folder for the SFDX project, if not retrieving a global value
     */
    static async getDefaultUsername(global: boolean = false, projectFolder: string) {
        let userName = await SFDCOrgs.getConfigValue(global, projectFolder, OrgConfigProperties.TARGET_ORG);
        if (!global && this.cachedUsername !== userName) {
            this.cachedUsername = userName;
        }
        return Promise.resolve(userName);
    }

    /**
     * Initializes the connection to Salesforce.
     * Loads the SFDX config file for the current workspace/project
     */
    static async initConnection(projectFolder: string) {
        const cachedUN = this.cachedUsername;
        const userName = await this.getDefaultUsername(false, projectFolder); // get the Default Username config value
        if (typeof this.cachedOrg === 'undefined' || cachedUN !== userName) {
            // if the default username has changed, make a new connection
            this.cachedOrg = await Org.create({
                aliasOrUsername: userName,
            });
        }
        return Promise.resolve(this.cachedOrg);
    }

    /**
     * Retrieves the list of authorized Orgs
     * @param aliasesOnly If true, only retrieves the aliases defined for Orgs, which is much quicker. If false, retrieves all information about all authorized Orgs.
     * @param refreshAuth If true, attempts to refresh the Authorization for each Org.
     */
    static async getConnectedOrgs(aliasesOnly: boolean, refreshAuth: boolean) {
        const orgLst: ConnectedOrg[] = [];
        // const info = await GlobalInfo.create({isGlobal: true});
        const allAuthorizations = await AuthInfo.listAllAuthorizations();
        await Promise.all(allAuthorizations.map(async (authorization) => {
            const alias = authorization.aliases[0];
            // const alias =
            //     info.aliases.get(authorization.username) ?? authorization.username;
            const userName: string = authorization.username;
            let org = new ConnectedOrg(alias, userName);
            if (aliasesOnly == false) {
                try {
                    const thisOrg = await Org.create({ aliasOrUsername: userName });
                    org.org = thisOrg;
                    org.connectionStatus = OrgConnectionStatus.UNKNOWN;
                    if (refreshAuth == true) {
                        try {
                            await thisOrg.refreshAuth();
                            org.connectionStatus = OrgConnectionStatus.CONNECTED;
                        } catch (refErr) {
                            org.connectionStatus = authorization.isExpired == true
                                ? OrgConnectionStatus.EXPIRED
                                : OrgConnectionStatus.DISCONNECTED;
                        }
                    }
                    if (authorization.isScratchOrg == true) {
                        org.orgType = OrgType.SCRATCH_ORG;
                    }
                    else {
                        const mdDescribe = await thisOrg
                            .getConnection()
                            .metadata.describe();

                        org.orgType = mdDescribe.testRequired
                            ? OrgType.PRODUCTION
                            : OrgType.SANDBOX;
                    }
                } catch (orgErr) {
                    org.connectionStatus = OrgConnectionStatus.EXPIRED;
                }
            }
            orgLst.push(org);
        }));
        return Promise.resolve(orgLst);
    }
}

/**
 * Represents information about an authorized Org
 */
export class ConnectedOrg {
    public alias: string;
    public username: string;
    public org: Org;
    public orgType: OrgType;
    public connectionStatus: OrgConnectionStatus;

    constructor(
        alias: string,
        username: string,
        org?: Org,
        orgType?: OrgType,
        connectionStatus?: OrgConnectionStatus
    ) {
        this.alias = alias;
        this.username = username;
        this.org = org;
        this.orgType = orgType;
        this.connectionStatus = connectionStatus;
    }
}

export enum OrgConnectionStatus {
    CONNECTED = "Connected",
    DISCONNECTED = "Disconnected",
    EXPIRED = "Expired",
    UNKNOWN = "Unknown"
}

export enum OrgType {
    PRODUCTION = "Production",
    SANDBOX = "Sandbox",
    SCRATCH_ORG = "Scratch Org"
}
