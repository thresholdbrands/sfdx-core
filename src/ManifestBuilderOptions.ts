/**
 * Options for the ManifestBuilder class
 */
export class ManifestBuilderOptions {
  // based on Metadata Coverage report for Spring '24 (v. 60.0)
  // https://developer.salesforce.com/docs/metadata-coverage/60
  public static readonly defaultExcludedTypes: string[] = [
    "AccessControlPolicy",
    "AccountInsightsSettings",
    "ActivationPlatformField", // unsupported by SDR registry
    "ActvPfrmDataConnectorS3", // unsupported by SDR registry
    "ActvPlatformAdncIdentifier", // unsupported by SDR registry
    "ActvPlatformFieldValue", // unsupported by SDR registry
    "AIAssistantTemplate",
    "AIConvSummarizationConfig",
    "AILiveMetricDefinition",
    "AnalysisConfiguration",
    "AnalyticsDataServicesSettings",
    "AppExplorationDataConsent",
    "AppleDomainVerification",
    "ArchiveSettings",
    "AssetSettings",
    "AttributeDefinition2",
    "CallCtrAgentFavTrfrDest",
    "CareProviderAfflRoleConfig",
    "CaseClassificationSettings",
    "ClaimFinancialSettings", // unsupported by SDR registry
    "CommonEventSubscription",
    "ConnectedSystem",
    "ConversationVendorFieldDef",
    "CustomDataType",
    "DataMapping",
    "DataMappingFieldDefinition",
    "DataMappingObjectDefinition",
    "DataMappingSchema",
    "DataPipeline",
    "DataSourceField", // unsupported by SDR registry
    "DecisionFlow",
    "DecisionFlowSubFlow",
    "DigitalExperienceBundleSetting",
    "DynamicTrigger",
    "EmbeddedServiceFieldService",
    "EncryptionKeySettings",
    "EntityImplements",
    "EssentialsTrialOrgSettings",
    "EventDelivery",
    "EventSubscription",
    "ExpressionSetObjectAlias", // unsupported by SDR registry
    "ExternalDataTranObject", // unsupported by SDR registry
    "ExternalServicesSettings",
    "FederationDataMappingUsage",
    "FeedFieldHistory",
    "Form",
    "FormSection",
    "GlobalPicklistValue",
    "IdentityVerificationProcDtl",
    "IdentityVerificationProcFld",
    "IndMfgSalesAgreementSettings",
    "IntegrationHubSettings",
    "IntegrationHubSettingsType",
    "InternalOrganization",
    "IsvHammerSettings",
    "JournalType",
    "LicenseDefinition",
    "LoginFlow",
    "MarketAudienceDefinition",
    "MarketingActionSettings",
    "MarketingResourceType",
    "MassMail",
    "MLDataDefinition",
    "MLPredictionDefinition",
    "MLRecommendationDefinition",
    "MobileSecurityPolicySet",
    "OmniExtTrackingDef", // unsupported by SDR registry
    "OmniTrackingComponentDef",
    "OmniTrackingGroup", // unsupported by SDR registry
    "OpportunityInsightsSettings",
    "Orchestration",
    "OrchestrationContext",
    "OrgPreferenceSettings",
    "OrgWideEmailAddress",
    "Package",
    "PardotTenant",
    "PaymentsIngestEnabledSettings",
    "PersonalJourneySettings",
    "PersonListSettings",
    "PlatformEncryptionSettings",
    // "Portal",
    "PortalDelegablePermissionSet", // unsupported by SDR registry
    "PrivateConnection",
    "ProductSpecificationTypeDefinition",
    "ReferencedDashboard", // unsupported by SDR registry
    "RelatedRecordAssocCriteria", // unsupported by SDR registry
    "ReportingTypeConfig",
    "SalesWorkQueueSettings", // unsupported by SDR registry
    "Scontrol",
    "ScoreRange",
    "SemanticDefinition",
    "SemanticModel",
    "SlackFeatureConfig",
    "SocialProfileSettings",
    "StnryAssetEnrgyUseCnfg",
    "SummaryLayout",
    "VehAssetEnrgyUseCnfg",
    "VirtualVisitConfig", // unsupported by SDR registry
    "VisualizationPlugin",
    "WaveAnalyticAssetCollection",
    "",
  ];
  public static readonly defaultOutputFile: string = "manifest/package.xml";

  constructor(
    readonly excludedTypes: string[] = ManifestBuilderOptions.defaultExcludedTypes,
    readonly outputFile: string = ManifestBuilderOptions.defaultOutputFile,
    readonly apiVersion?: string
  ) {}
}
