import * as differ from "diff";
import simpleGit, { BranchSummary, SimpleGit } from "simple-git";

export class GitBranchFileAggregator {

  private excludedFiles: string[] = [
    "bitbucket-pipelines.yml",
    "package.xml",
    "server.key.enc",
    "project-scratch-def.json",
    ".gitignore",
    ".diff",
    ".forceignore",
    ".mdignore",
    ".prettierrc",
    ".pmdcache",
    "sfdx-project.json",
  ];

  private git: SimpleGit;

  /**
   *
   * @param currentWorkingDirectory the directory where the git repository lives
   */
  constructor(
    readonly currentWorkingDirectory: string
  ) {
    this.git = simpleGit(currentWorkingDirectory);
  }

  async getActiveLocalBranches(): Promise<BranchSummary> {
		return await this.git.branchLocal();
  }

  async execute(
    branchToCompare: string
  ): Promise<Map<string, string[]>> {

    const revList1:string = await this.git.raw(['rev-list', '--first-parent', branchToCompare]);
    const revList2:string = await this.git.raw(['rev-list', '--first-parent', 'HEAD']);
    const revDiff:differ.Change[] = differ.diffLines(revList1, revList2);
    let ancestor: string = revDiff
      .reduce((prev, curr, index, arr) => {
        if (prev !== undefined) {
          return prev;
        }
        return !curr.added && !curr.removed ? curr : undefined;
      }, undefined)
      .value.split('\n')[0];
    ancestor = await this.git.raw(['rev-parse', '--short', ancestor]);
    ancestor = ancestor.trim();

    const stagedFiles:string[] = (await this.git.diff(["--name-only", "--cached", "--diff-filter=d"])).split('\n').filter(this.filterExcludedFiles).filter(file => file.trim() !== '');
    const untrackedFiles:string[] = (await this.git.raw(["ls-files", "-m", "-o", "--exclude-standard"])).split('\n').filter(this.filterExcludedFiles).filter(file => file.trim() !== '');
    const modifiedCommittedFiles:string[] = (await this.git.diff(["-m", "--no-commit-id", "--name-only", "--diff-filter=d", "-r", `${ancestor}...HEAD`])).split('\n').filter(this.filterExcludedFiles).filter(file => file.trim() !== '');

    let ret = new Map<string, string[]>();
    ret.set('staged', stagedFiles);
    ret.set('untracked', untrackedFiles);
    ret.set("committed", modifiedCommittedFiles);
    return Promise.resolve(ret);
  }

  public filterExcludedFiles = (file:string): boolean => {
    return this.excludedFiles.some((excluded:string) => {
      return !file.endsWith(excluded);
    });
  }
}
