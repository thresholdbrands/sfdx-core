# 6.0.0 - 2024-01-18
- Update dependency versions
  - @salesforce/core 5.2.1 => 6.4.7
  - simple-git 3.19.1 => 3.22.0
- update required node version from 18 => 20
- Update Salesforce Metadata type coverage to API v60.0

# 5.0.4 - 2023-08-19
- Bump @salesforce/core library dependency from 5.2.0 to 5.2.1

# 5.0.3 - 2023-08-07
- Update Standard Value Set member list

# 5.0.2 - 2023-08-07
- Update wildcard-supported metadata types

# 5.0.1 - 2023-08-06
- Update Salesforce Metadata type coverage to API v58.0

# 5.0.0 - 2023-08-06
- Updated @salesforce/core library dependency to v3
- Updated all outdated dependencies

# 3.0.8 - 2023-08-06
- Updated @salesforce/core library dependency to v3
- Updated all outdated dependencies
- Requires Node >= 16
- Update Salesforce Metadata type coverage to API v53.0
- Switched from yarn to npm
# 2.1.1 - 2021-07-29

- Renamed package from `@zervice/zervice-sfdx-core` to `@thresholdbrands/sfdx-core` to reflect new company name

# 2.1.0 - 2020-09-26

- Added constructor to `GitBranchFileAggregator` for better OOD
- Added `getActiveLocalBranches` method to `GitBranchFileAggregator` for retrieving local branches
- Updated simple-git usage to use `import` instead of `require`
- Updated @salesforce/core library to v2.12.1 for updated functionality

# 2.0.1 - 2020-09-19

- Removed `Portal` from default excluded metadata types when building a project manifest
- Added `PermissionSetGroup` to list of metadata types that support a wildcard
- Added `branchToCompare` parameter to `GitBranchFileAggregator.execute` method for more flexible usage

# 2.0.0 - 2020-08-10

- Updated dependencies
- Renamed `SFDCConnectionBuilder` to `SFDCOrgs`
  - Added `getConfigValue`, `getDefaultDevHubUsername`, `getDefaultUsername`, and `getConnectedOrgs` methods
  - Made methods static for more simplistic usage
- Added `GitBranchFileAggregator` class for additional functionality

# 1.0.0 - 2020-06-15

- Initial release
- Refactored command logic from `@zervice/zervice-sfdx-plugins` package