# sfdx-core

Core functionality used in other Threshold Brands packages. No guarantees on support or updates.

## Contribute

PRs are welcome!

## Usage

```sh-session
$ npm install @thresholdbrands/sfdx-core

USAGE
import * as zCore from "@thresholdbrands/sfdx-core"
```

## Classes

# `SFDCOrgs`

Provides static methods for Salesforce Org management.

### Methods

#### `getConfigValue`

> Retrieves a value from the local project or global configuration
>
> `EXAMPLE`
>
> ```
> const defaultProjectUN:string = await zCore.SFDCOrgs.getConfigValue(false, '/path/to/sfdx/project', 'defaultusername');
> const defaultGlobalUN:string = await zCore.SFDCOrgs.getConfigValue(true, null, 'defaultusername');
> ```

#### `getDefaultDevHubUsername`

> Retrieves the default Dev Hub Username for the local project or global configuration
>
> `EXAMPLE`
>
> ```
> const defaultProjectDevHubUN:string = await zCore.SFDCOrgs.getDefaultDevHubUsername(false, '/path/to/sfdx/project');
> const defaultGlobalDevHubUN:string = await zCore.SFDCOrgs.getDefaultDevHubUsername(true, null);
> ```

#### `getDefaultUsername`

> Retrieves the default Username for the local project or global configuration
>
> `EXAMPLE`
>
> ```
> const defaultProjectUN:string = await zCore.SFDCOrgs.getDefaultUsername(false, '/path/to/sfdx/project');
> const defaultGlobalUN:string = await zCore.SFDCOrgs.getDefaultUsername(true, null);
> ```

#### `initConnection`

> Initializes a connection to an authorized org. Caches the connection for improved performance.
>
> `USAGE`
>
> ```
> import { Org } from "@salesforce/core";
> import * as tbCore from "@thresholdbrands/sfdx-core"
> ```
>
> `EXAMPLE`
>
> ```
> const org:Org = await zCore.SFDCOrgs.initConnection('/path/to/sfdx/project');
> ```

#### `getConnectedOrgs`

> Retrieves information about all authorized orgs
>
> `EXAMPLE`
>
> ```
> const aliases:zCore.ConnectedOrg[] = await zCore.SFDCOrgs.getConnectedOrgs(true, false);
> const orgs:zCore.ConnectedOrg[] = await zCore.SFDCOrgs.getConnectedOrgs(false, false);
> ```

# `ManifestBuilderOptions`

Options for the ManifestBuilder class

`PROPERTIES`

|apiVersion|String|API Version to use for the connection to Salesforce||
|excludedTypes|String[]|Metadata Types to exclude from the manifest|ManagedContentType (not currently supported to be retrieved using Salesforce CLI)|
|outputFile|String|File path and name to output the manifest to|"manifest/package.xml"|

`EXAMPLE`

```
const options:zCore.ManifestBuilderOptions = new zCore.ManifestBuilderOptions(['ManagedContentType'], 'manifest/package.xml', '49.0');
const builder:zCore.ManifestBuilder = new zCore.ManifestBuilder(options);
```

# `ManifestBuilder`

Retrieves Metadata types and creates/updates an XML manifest file

`EXAMPLE`

```
const logMessage = (msg) => { console.log(msg); }
const sfOrg = await zCore.SFDCOrgs.initConnection('/path/to/sfdx/project');
const options:zCore.ManifestBuilderOptions = new zCore.ManifestBuilderOptions();
const builder:zCore.ManifestBuilder = new zCore.ManifestBuilder(options, sfOrg, logMessage);
await builder.buildManifest();
```

# `GitBranchFileAggregator`

Provides methods for common git functionality.

### Methods

#### `execute`

> Aggregates files into a map of lists categorized by Git status
>
> `EXAMPLE`
>
> ```
> const agg:zCore.GitBranchFileAggregator = new zCore.GitBranchFileAggregator(projectDir);
> const files:Map<string, string[]> = await agg.execute();
> ```

#### `getActiveLocalBranches`

> Retrieves information about local Git branches.
>
> `EXAMPLE`
>
> ```
> const agg:zCore.GitBranchFileAggregator = new zCore.GitBranchFileAggregator(projectDir);
> const branches:BranchSummary = await agg.getActiveLocalBranches();
> ```
